<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorktable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('transfer', function(Blueprint $table) {
            $table->increments('id');

            $table->string('id_shop', 50)->index();
            $table->string('id_order', 50)->index();
            $table->string('status_order', 20)->nullable()->index();
            $table->decimal('sum_order', 8, 2)->nullable()->index();
            $table->string('currency', 10)->nullable()->index();
            $table->dateTime('date_order')->nullable()->index();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('transfer');
    }
}
