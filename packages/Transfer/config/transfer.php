<?php
return[
    'destination'   => storage_path('transfer_files'),

    /* ---------------------------------------------- */
    'suppliers'         => [

        /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        'ebay'          => [
            'name'      => 'eBay',
            'dir'       => 'ebay',
            'file'      => [
                'name'      => 'csv_1.csv',
                'format'    => 'csv',
                'structure' => [
                    'delimiter' => "\t",
                ],
            ],
            'data'      => [
                'id_shop'       => 4,
                'id_order'      => 18,
                'status_order'  => '',
                'sum_order'     => 15,
                'currency'      => '',
                'date_order'    => 11,
                'filter'        => [
                    'field'     => 2,
                    'value'     => 'Winning Bid (Revenue)',
                    'condition' => '==',
                ]
            ],
        ],
        
        /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
        'aliexpress'    => [
            'name'      => 'Aliexpress INT',
            'dir'       => 'aliexpress',
            'file'      => [
                'name'      => 'xml_1.xml',
                'format'    => 'xml',
                'structure' => [
                'parent_node'   => "stat",
                ],
            ],
            'data'      => [
                'id_shop'       => 'advcampaign_id',
                'id_order'      => 'order_id',
                'status_order'  => 'status',
                'sum_order'     => 'cart',
                'currency'      => 'currency',
                'date_order'    => 'action_date',
                'filter'        => null,
            ],
        ],
    ],
];

/*
 * Описание структуры xml
<advcampaign_id> - id магазина
<order_id> - id заказа в магазине
<status> - статус заказа
<cart> - сумма заказа
<currency> - валюта
<action_date> - время создания заказа

Описание структуры csv
"1" - id магазина
Unique Transaction ID - id заказа в магазине
"approved" - статус заказа
eBay Total Sale Amount - сумма заказа
"USD" - валюта
Click Timestamp - время создания заказа
Импортировать только те у которых Event Type == Winning Bid (Revenue)
 */