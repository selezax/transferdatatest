<?php

namespace Transfer\Readers;

use Transfer\Contracts\ReadersInterface;
use Illuminate\Support\Facades\File;

abstract class Readers implements ReadersInterface
{
    protected $fileName;
    protected $dirName;
    protected $fileOptions;
    protected $dataOptions;
    protected $filter;


    public function setFileName($file){
        $this->fileName = $file;
        return $this;
    }

    public function setDirectoryName($dir){
        $this->dirName = $dir;
        return $this;
    }

    public function setFileOptions(array $options = []){
        $this->fileOptions = $options;
        return $this;
    }

    public function setDataOptions(array $options = []){
        $this->dataOptions = $options;
        $this->chkFilter();
        return $this;
    }


    /**
     *
     */
    protected function chkFilter(){
        if( isset($this->dataOptions['filter']) &&
            ($this->dataOptions['filter']['field'] && $this->dataOptions['filter']['value'] && $this->dataOptions['filter']['condition'])){
            $this->filter = [
                'field'     => $this->dataOptions['filter']['field'],
                'value'     => $this->dataOptions['filter']['value'],
                'condition' => $this->dataOptions['filter']['condition'],
            ];

        }
        else
            $this->filter = false;
    }

}