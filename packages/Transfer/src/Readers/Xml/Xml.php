<?php

namespace Transfer\Readers\Xml;

use Transfer\Readers\Readers;
use Exception;
use XMLReader;

class Xml extends Readers
{
    protected $document = null;
    protected $ftell = null;

//    public function __destruct()
//    {
//        $this->fileClose();
//    }

    /**
     * @return $this
     * @throws Exception
     *
     * Open the data XML file
     */
    public function fileOpen(){
        $file_name = config('transfer.destination') . DIRECTORY_SEPARATOR . $this->dirName . DIRECTORY_SEPARATOR . $this->fileName ;
        $this->document = new XMLReader();
        if ( $this->document->open($file_name) === FALSE) throw new Exception('Error file open: ' . $file_name);
        return $this;
    }

    /**
     * @return $this
     * CSV file close
     */
    public function fileClose(){
        return $this;
    }

    /**
     * @param int $limit / The number lines for one request
     * @return array
     * @throws Exception
     *
     * Get data from XML file
     */
    public function getData($limit = null){
        $dataXml = [] ;
        $i = 0;

        while($this->document->read()) {
            if ($this->document->nodeType == XMLReader::ELEMENT && $this->document->localName == $this->fileOptions['parent_node']) {
                $current_key = false;

                while ($this->document->read()){
                    if($this->document->nodeType == XMLReader::END_ELEMENT) continue;
                    if($this->document->name == $this->fileOptions['parent_node']) break;

                    if($_ck = array_search($this->document->localName, $this->dataOptions)){
                        $current_key = $_ck ;
                        continue;
                    }

                    if($current_key && $this->document->nodeType == XMLReader::TEXT){
                        $dataXml[$i][$current_key] = $this->document->value;
                        $current_key = false;
                    }

                }

                if(++$i == $limit) break;
            }
        }

        return $dataXml;

    }
 
    /**
     * @param int $n
     * @param callable $callback
     * @return bool
     */
    public function chunk($n, callable $callback) {
        $results = $this->getData($n);

        while ( count($results) > 0) {
            if (call_user_func($callback, $results) === false) {
                return false;
            }
            $results = $this->getData($n);
        }

        return true;
    }


}