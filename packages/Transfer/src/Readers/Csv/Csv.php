<?php

namespace Transfer\Readers\Csv;

use Transfer\Readers\Readers;
use Exception;

class Csv extends Readers
{
    protected $handle_f = null;
    //protected $ftell = null;

    public function __destruct()
    {
        $this->fileClose();
    }

    /**
     * @return $this
     * @throws Exception
     *
     * Open the data CSV file
     */
    public function fileOpen(){
        $file_name = config('transfer.destination') . DIRECTORY_SEPARATOR . $this->dirName . DIRECTORY_SEPARATOR . $this->fileName ;
        if (($this->handle_f = fopen($file_name, "r")) === FALSE) throw new Exception('Error file open: ' . $file_name);
        return $this;
    }

    /**
     * @return $this
     * CSV file close
     */
    public function fileClose(){
        if($this->handle_f)
            @fclose($this->handle_f);
        return $this;
    }

    /**
     * @param int $limit / The number lines for one request
     * @return array
     * @throws Exception
     *
     * Get data from CSV file
     */
    public function getData($limit = null){
        if(!$this->handle_f) throw new Exception('Error. File not open. First, open the file fileOpen()');
        $csvData = array();
        $i = 0;

        while ( ($_data_f = fgets($this->handle_f)) !== FALSE) {

            $data_f = explode($this->fileOptions['delimiter'],$_data_f);

            if($this->filter){
                $_filter = sprintf("'%s' %s '%s'", $data_f[$this->filter['field']], $this->filter['condition'], $this->filter['value']) ;
                if(!eval( "return $_filter ;")) continue ;
            }

            $csvData[] = [
                'id_shop'       => $data_f[$this->dataOptions['id_shop']] ,
                'id_order'      => $data_f[$this->dataOptions['id_order']] ,
                // 'status_order'  => '',
                'sum_order'     => $data_f[$this->dataOptions['sum_order']],
                // 'currency'      => '',
                'date_order'    => $data_f[$this->dataOptions['date_order']],
            ];

            if($limit && $limit == ++$i){
                //$this->ftell = ftell($this->handle_f);
                break;
            }
        }
 
        return $csvData;
    }


    /**
     * @param int $n
     * @param callable $callback
     * @return bool
     */
    public function chunk($n, callable $callback) {

        $this->fileOpen();
        $results = $this->getData($n);

        while ( count($results) > 0) {
            if (call_user_func($callback, $results) === false) {
                return false;
            }
            $results = $this->getData($n);
        }

        unset($results);
        $this->fileClose();
        return true;
    }


}