<?php

namespace Transfer\Console\Commands;

use Illuminate\Console\Command;
use Transfer\Models\Transfer;
use Transfer\Facades\CsvReader;
use Transfer\Facades\XmlReader;
use Carbon\Carbon;

class consoleTransfer extends Command {

    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'transfer:start';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Transfer data from files to DB';
 

    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle() {

        $this->info('Start process transfer');
        $this->line('Current memory usage: ' . memory_get_usage());

        foreach (config('transfer.suppliers') as $supplier){
            $this->info(PHP_EOL . 'Start process for ' . $supplier['name'] . PHP_EOL);
            $start = microtime(true);

            $currentMethod = $supplier['file']['format'];
            $this->$currentMethod($supplier);

            $this->line(PHP_EOL . 'Current memory usage: ' . memory_get_usage());
            $this->line('Elapsed time: ' . (microtime(true) - $start) . ' s.');

        }

        $this->info(PHP_EOL . 'Process transfer completed.');
        $this->line('Current memory usage: ' . memory_get_usage());

    }

    /**
     * @param array $data
     * @return bool
     *
     * data to DB
     */
    protected function actionToDb(array $data = []){
        if(empty($data['id_shop']) && empty($data['id_order'])) return false ;

        $pitem = Transfer::firstOrNew([
                    'id_shop' => $data['id_shop'],
                    'id_order' => $data['id_order']
                ]);

        $pitem->status_order    = isset($data['status_order'])?$data['status_order']:null;
        $pitem->currency        = isset($data['currency'])?$data['currency']:null;

        $pitem->sum_order       = isset($data['sum_order'])?$data['sum_order']:null;
        $pitem->date_order      = isset($data['date_order'])?Carbon::parse($data['date_order'])->toDateTimeString():null;
        $pitem->save();

        return true;
    }


    /**
     * @param $suplier
     * @return bool
     *
     * Service read CSV
     */
    protected function csv(array $suplier){

        CsvReader::setFileName(array_get($suplier, 'file.name'))
            ->setDirectoryName(array_get($suplier, 'dir'))
            ->setFileOptions(array_get($suplier, 'file.structure'))
            ->setDataOptions(array_get($suplier, 'data'))
        ;

        CsvReader::chunk(100, function($dataCsv){
            // file_put_contents('/tmp/test.php', print_r($dataCsv, true), FILE_APPEND);
            foreach ($dataCsv as $item){
                $this->actionToDb($item);
                echo '|';
            }
        });

        return true;
    }

    protected function xml($suplier){

        XmlReader::setFileName(array_get($suplier, 'file.name'))
            ->setDirectoryName(array_get($suplier, 'dir'))
            ->setDataOptions(array_get($suplier, 'data'))
            ->setFileOptions(array_get($suplier, 'file.structure'))
            ->fileOpen();
        ;

        XmlReader::chunk(50, function($dataCsv){
            foreach ($dataCsv as $item){
                $this->actionToDb($item);
                echo '|';
            }
        });

        return true;
    }


}