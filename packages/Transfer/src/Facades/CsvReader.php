<?php

namespace Transfer\Facades;

use Illuminate\Support\Facades\Facade;
use Transfer\Contracts\ReadersInterface;

class CsvReader extends Facade
{
    public static function getFacadeAccessor()
    {
        return 'csv';
    }

}