<?php

namespace Transfer\Facades;

use Illuminate\Support\Facades\Facade;
use Transfer\Contracts\ReadersInterface;

class XmlReader extends Facade
{
    public static function getFacadeAccessor()
    {
        return 'xml';
    }

}