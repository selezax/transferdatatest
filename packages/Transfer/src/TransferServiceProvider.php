<?php

namespace Transfer;

use Illuminate\Support\ServiceProvider;

class TransferServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->publishes([__DIR__ . '/../config/transfer.php'  => ('transfer.php')], 'transfer');

        $this->app->singleton('csv', function ($app) {
            return new Readers\Csv\Csv();
        });

        $this->app->singleton('xml', function ($app) {
            return new Readers\Xml\Xml();
        });

    }

    public function boot()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/transfer.php', 'transfer');

        class_alias(\Transfer\Facades\CsvReader::class, 'Csv');
        class_alias(\Transfer\Facades\XmlReader::class, 'Xml');

    }

}