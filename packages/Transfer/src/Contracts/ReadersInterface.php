<?php 

namespace Transfer\Contracts;


interface ReadersInterface
{

    public function setFileName($file);
    public function setDirectoryName($dir);
    public function setFileOptions(array $options = [] );
    public function setDataOptions(array $options = [] );

}