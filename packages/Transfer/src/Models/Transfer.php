<?php

namespace Transfer\Models;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    protected $table = 'transfer';

    protected $fillable = [
        'id_shop',
        'id_order',
        'status_order',
        'sum_order',
        'currency',
        'date_order'
    ];

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
    ];

    /** @TODO: create rules */
//    public static $rules = [];

   

}
