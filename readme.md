## Инсталяция
**выполнить в консоли**

composer update

*будут устанавливаться пакеты фреймворка*


## Файл конфигурации
### Настройки фреймворка
скопировать 
**.env.example** to **.env**
```
APP_ENV=local
APP_DEBUG=true
APP_KEY=HKgGL5TwbDdyiLGC4DyX5BjWMHsjKUkd
DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=_database_name_
DB_USERNAME=_user_name_
DB_PASSWORD=_password_
```

### Настройки пакета
```
packages/Transfer/config/transfer.php
```
> разместить файлы импорта, по умолчанию используются 
> 
>storage/transfer_files/aliexpress/xml_1.xml
>
>storage/transfer_files/ebay/csv_1.csv 

#### destination
директория где будут храниться файлы для обработки

#### suppliers
список поставщиков для импорта

+ **ключ** - произвольное уникальное значение
  + **name** - произвольное значение (информативный характер)
  + **dir** - субдиректория в destination где храняться файлы импорта, непосредственно для текущего поставщика 
  + **file** - настройки файла импорта
    + **name** - имя файла в destination/dir
    + **format** - формат файла для обработки (csv, xml, etc ...)
    + **structure** - опции для распарсинга файла
      + delimiter' => "\t", (для примера, текущий файл в формате CSV, для разделения столбцов используется табуляция)

  + **data** - опции для распарсинга данных
    + 'id_shop' - список полей для составления соответствия с таблицей 
    + 'id_order'
    + 'status_order'
    + 'sum_order'
    + 'currency'
    + 'date_order'
    + **filter** - настройки для выборки данных из файла
      + **field** - наименование поля
      + **value** - значение поля 
      + **condition** - условие


## Рабочая таблица
**выполнить в консоли**
```
php artisan migrate
php artisan migrate  --path=packages/Transfer/database/migrations/
```

## Запуск из консоли
```
php artisan transfer:start
```

## Cron
Команда для cron
```
* * * * * php /path/to/artisan schedule:run >> /dev/null 2>&1
```
Запускает механизм планирования фреймворка.

```
app/Console/Kernel.php
```
```php 
$schedule->command('transfer:start')->everyFiveMinutes()->withoutOverlapping();
```
указывает, что transfer:start будет выполняться everyFiveMinutes (каждые 5 минут)

withoutOverlapping() - предотвращает запуск, если предыдущий запуск еще не завершён. 